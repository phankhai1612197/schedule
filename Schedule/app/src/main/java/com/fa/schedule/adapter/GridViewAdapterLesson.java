package com.fa.schedule.adapter;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.fa.schedule.R;

import java.util.ArrayList;

public class GridViewAdapterLesson extends BaseAdapter {

    private Context mContext;

    private ArrayList<String> mList;

    private boolean flag;

    public GridViewAdapterLesson(Context mContext, ArrayList<String> mList, boolean flag) {
        this.mContext = mContext;
        this.mList = mList;
        this.flag = flag;
    }

    @Override
    public int getCount() {
        return this.mList.isEmpty() ? 0 : this.mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            //initView
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_lesson, parent, false);
            //init Textview item
            viewHolder.mTv = convertView.findViewById(R.id.item_tv_lesson);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //set data for textview
        viewHolder.mTv.setText(mList.get(position));
        if (!flag) {
            viewHolder.mTv.setOnTouchListener(new MyTouchListener());
            viewHolder.mTv.setOnDragListener(new MyDragListener());
        }
        return convertView;
    }

    class ViewHolder {
        private TextView mTv;
    }

    final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                ClipData data = ClipData.newPlainText("", "");

                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

                view.startDrag(data, shadowBuilder, view, 0);

                view.setVisibility(View.VISIBLE);

                return true;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:

//                    View view = (View) event.getLocalState();
//                    TextView dropTarget = (TextView) v;
//                    TextView dropped = (TextView) view;
//                    if (v.getId() != ) {
//                        if (dropTarget.getText().toString().charAt(0) != dropped.getText().toString().charAt(0)) {
//                            view.setVisibility(View.VISIBLE);
//                            dropTarget.setText(dropped.getText().toString());
//                            dropTarget.setTypeface(Typeface.DEFAULT_BOLD);
////                        Object tag = dropTarget.getTag();
//                            dropTarget.setTag(dropped.getId());
//                        } else {
//                            Toast.makeText(mContext, dropTarget.getText().toString() + "is not " + dropped.getText().toString(), Toast.LENGTH_LONG).show();
//                        }
//                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
