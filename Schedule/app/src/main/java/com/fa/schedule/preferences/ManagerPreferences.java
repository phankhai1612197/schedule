package com.fa.schedule.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class ManagerPreferences {
    private Context context;
    private SharedPreferences preferences;

    public ManagerPreferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences("Data", Context.MODE_PRIVATE);
    }

    public void setOption(boolean value) {
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean("cb1", value).commit();
    }

    public boolean getOption() {
        return preferences.getBoolean("cb1", false);
    }

    public void clearAll() {
        preferences.edit().clear().commit();
    }
}
