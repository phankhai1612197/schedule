package com.fa.schedule.adapter;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Typeface;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fa.schedule.R;
import com.fa.schedule.inteface.OnChangeData;

import java.util.ArrayList;

public class GridViewAdapterSchedule extends BaseAdapter {
    private Context mContext;

    private static int index = 0;
    private ArrayList<String> mList;

    onItemClick itemClick;

    OnChangeData changeData;

    public GridViewAdapterSchedule(Context mContext, ArrayList<String> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    /*
    p1 is postion old
    p2 is postion new
     */
    public interface onItemClick {

        void setOnItemSchedule(int p1, int p2, String value);

        void setOnItemLesson(int postion, String value);
    }

    public void setOnItemClick(onItemClick onItemClick) {
        this.itemClick = onItemClick;
    }

    @Override
    public int getCount() {
        return this.mList.isEmpty() ? 0 : this.mList.size();
    }

    public void setOnChaneData(OnChangeData onChaneData) {
        this.changeData = onChaneData;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            //initView
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_schedule, parent, false);
            //init Textview item
            viewHolder.mTv = convertView.findViewById(R.id.item_tv_schedule);
            //event click item in gridview
            viewHolder.mTv.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            ClipData data = ClipData.newPlainText("", "");
                            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                            v.startDrag(data, shadowBuilder, v, 0);
                            v.setVisibility(View.VISIBLE);
                            index = position;
                            break;
                    }
                    return false;
                }
            });
            viewHolder.mTv.setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View v, DragEvent event) {
                    switch (event.getAction()) {
                        case DragEvent.ACTION_DRAG_STARTED:
                            break;
                        case DragEvent.ACTION_DRAG_ENTERED:
                            break;
                        case DragEvent.ACTION_DRAG_EXITED:
                            break;
                        case DragEvent.ACTION_DROP:

                            //show button ok and cancel
                            changeData.setOnChangeData(true);

                            View view = (View) event.getLocalState();
                            //view new
                            TextView dropTarget = (TextView) v;
                            //view old
                            TextView dropped = (TextView) view;
                            //set text for item new
//                            dropTarget.setText(dropped.getText().toString());

                            dropTarget.setTypeface(Typeface.DEFAULT_BOLD);
                            if (v.getId() == dropped.getId()) {
                                itemClick.setOnItemSchedule(index, position, dropped.getText().toString());
                            } else {
                                itemClick.setOnItemLesson(position, dropped.getText().toString());
                            }
                            break;
                        case DragEvent.ACTION_DRAG_ENDED:
                            //no action necessary
                            break;
                        default:
                            break;
                    }
                    return true;
                }
            });
//            viewHolder.mTv.setOnDragListener(new MyDragListener(position));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //set data for textview
        viewHolder.mTv.setText(mList.get(position));

        //when textview ="" then not envent

        return convertView;
    }

    class ViewHolder {
        private TextView mTv;
    }
}
