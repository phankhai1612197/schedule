package com.fa.schedule.widget;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.RemoteViewsService;

import com.fa.schedule.database.DataBase;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author duanhd
 */
public class WidgetService extends RemoteViewsService {
    private final String[] WEEK_DAY = {"MON", "TUE", "WED", "THU", "FRI", "SAT"};

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new WidgetViewFactory(getWeekData(), getApplicationContext());
    }

    public List<String> getWeekData() {
        return getSchedule(setKey());
    }

    public List<String> getSchedule(String value) {
        final String DB_NAME = "db_schedule.sqlite";
        List<String> scheduleList = new ArrayList<>();
        SQLiteDatabase database = DataBase.initDatabase(getApplicationContext(), DB_NAME);
        Cursor cursor = database.rawQuery("SELECT * FROM tbl_schedule WHERE date=?", new String[]{value.trim()});
        cursor.moveToFirst();

//        if (cursor.getCount() == 0) {
//            try {
                int j = 0;
//                JSONArray jsonArray = new JSONArray(cursor.getString(1));

                for (int i = 0; i < 49; i++) {
                    if (i == 0) {
                        scheduleList.add("");
                    } else if (i < 7) {
                        scheduleList.add(WEEK_DAY[i - 1]);
                    } else if (i % 7 == 0) {
                        scheduleList.add("Lesson " + (i / 7));
                    } else {
                        if (cursor.getCount() > 0) {
                            scheduleList.add("");
                        } else {
                            scheduleList.add("");
                        }
                    }
                }

//        }
        cursor.close();
        return scheduleList;
    }

    public String setKey() {
        String dateFormat = "dd/MM/yyyy";
        Calendar c = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());

        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        Date firstDayOfWeek = c.getTime();
        sb.append(sdf.format(firstDayOfWeek));
        sb.append(" - ");

        c.add(Calendar.DAY_OF_WEEK, 5);
        Date lastDayOfWeek = c.getTime();
        sb.append(sdf.format(lastDayOfWeek));

        return sb.toString();
    }
}
