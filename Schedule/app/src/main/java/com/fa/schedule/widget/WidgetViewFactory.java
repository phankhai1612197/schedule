package com.fa.schedule.widget;

import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.fa.schedule.R;

import java.util.List;

/**
 * @author duanhd
 */
public class WidgetViewFactory implements RemoteViewsService.RemoteViewsFactory {
    private List<String> mItems;
    private Context mContext;

    public WidgetViewFactory(List<String> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews row = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);

        row.setTextViewText(R.id.widget_item, mItems.get(position));

        Intent intent = new Intent();
        row.setOnClickFillInIntent(R.id.widget_item, intent);

        return row;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
