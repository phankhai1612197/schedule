package com.fa.schedule.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.fa.schedule.R;
import com.fa.schedule.activity.MainActivity;

/**
 * @author duanhd
 */
public class ScheduleWidget extends AppWidgetProvider {
    public static final String ACTION_DATA_UPDATE = "DATA_UPDATE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int widgetId : appWidgetIds) {
            RemoteViews widget = new RemoteViews(context.getPackageName(), R.layout.schedule_widget);
            widget.setTextViewText(R.id.widget_title, context.getResources().getString(R.string.app_name));

            Intent svcIntent = new Intent(context, WidgetService.class);
            widget.setRemoteAdapter(widgetId, R.id.widget, svcIntent);

            Intent intentUpdate = new Intent(context, ScheduleWidget.class);
            intentUpdate.setAction(ACTION_DATA_UPDATE);
            PendingIntent pendingUpdate = PendingIntent.getBroadcast(context, 0, intentUpdate, PendingIntent.FLAG_UPDATE_CURRENT);
            widget.setOnClickPendingIntent(R.id.widget_title, pendingUpdate);

            Intent clickIntent = new Intent(context, MainActivity.class);
            PendingIntent clickPI = PendingIntent.getActivity(context, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            widget.setPendingIntentTemplate(R.id.widget, clickPI);

            appWidgetManager.updateAppWidget(appWidgetIds, widget);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (action != null && action.equalsIgnoreCase(ACTION_DATA_UPDATE)) {
            /*ComponentName cn = new ComponentName(context, ScheduleWidget.class);
            AppWidgetManager awm = AppWidgetManager.getInstance(context);
            int[] appWidgetId = awm.getAppWidgetIds(cn);
            RemoteViews widget = new RemoteViews(context.getPackageName(), R.layout.schedule_widget);
            awm.updateAppWidget(appWidgetId, widget);
            awm.notifyAppWidgetViewDataChanged(appWidgetId, R.id.widget);*/

            Toast.makeText(context, "Update", Toast.LENGTH_SHORT).show();
        }
    }
}
