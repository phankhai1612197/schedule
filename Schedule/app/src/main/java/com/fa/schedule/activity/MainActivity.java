package com.fa.schedule.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Html;
import android.text.InputType;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fa.schedule.R;
import com.fa.schedule.adapter.GridViewAdapterLesson;
import com.fa.schedule.adapter.GridViewAdapterSchedule;
import com.fa.schedule.database.DataBase;
import com.fa.schedule.inteface.OnChangeData;
import com.fa.schedule.preferences.ManagerPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GridViewAdapterSchedule.onItemClick,
        View.OnClickListener, AdapterView.OnItemClickListener, OnChangeData {


    public static final String DATABASE_NAME = "db_schedule.sqlite";

    private GridView mGridViewSchedule, mGridViewLesson;

    //list contain schedule
    private ArrayList<String> mList;

    private ArrayList<String> mListCycle;

    private ArrayList<String> mListLesson;

    private ImageView mImgPrevious, mImgNext, mImgRecycle;

    private GridViewAdapterSchedule mAdapterSchedule;

    private GridViewAdapterLesson mAdapterLesson;

    private TextView mTvAdd, mTvUpdate, mTvOk, mTvCancel, mTvCalendar;

    private LinearLayout mLayoutSchedule, mLayoutdb;

    private boolean isUpdate = false;

    private static float hide_view = 0.3f;
    private static float show_view = 1.0f;

    private SQLiteDatabase mDb;

    private Calendar mDateNow = Calendar.getInstance();

    private ManagerPreferences preferences;

    private static final int DURATION = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDb = DataBase.initDatabase(MainActivity.this, DATABASE_NAME);

        preferences = new ManagerPreferences(MainActivity.this);
        //initView
        mGridViewSchedule = findViewById(R.id.gridViewSchedule);

        mGridViewLesson = findViewById(R.id.gridViewLesson);
        //textview add
        mTvAdd = findViewById(R.id.tv_add);
        mTvAdd.setOnClickListener(this);
        //textview update
        mTvUpdate = findViewById(R.id.tv_update);
        mTvUpdate.setOnClickListener(this);
        //Textview ok
        mTvOk = findViewById(R.id.tv_ok);
        mTvOk.setOnClickListener(this);
        //Textview Cancel
        mTvCancel = findViewById(R.id.tv_cancel);
        mTvCancel.setOnClickListener(this);


        //Layout schedule
        mLayoutSchedule = findViewById(R.id.layout_schedule);

        //ImageView previous
        mImgPrevious = findViewById(R.id.img_previous);
        mImgPrevious.setOnClickListener(this);

        //ImageView next
        mImgNext = findViewById(R.id.img_next);
        mImgNext.setOnClickListener(this);

        //ImageView reycleview
        mImgRecycle = findViewById(R.id.img_recycle);
//        mImgRecycle.setOnTouchListener(new MyTouchListener());
        mImgRecycle.setOnDragListener(new DragToTrashListener());

        mTvCalendar = findViewById(R.id.tv_calendar);
        mTvCalendar.setOnClickListener(this);

        mLayoutdb = findViewById(R.id.layout_btn_db);
        mList = new ArrayList<>();
        mListCycle = new ArrayList<>();
        mListLesson = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            mListLesson.add("");
        }

        for (int i = 0; i < 36; i++) {
            mList.add("");
            mListCycle.add("");
        }
        mAdapterSchedule = new GridViewAdapterSchedule(MainActivity.this, mList);
        mGridViewSchedule.setAdapter(mAdapterSchedule);
        mGridViewSchedule.setOnItemClickListener(this);
        mAdapterSchedule.setOnItemClick(this);
        mAdapterSchedule.setOnChaneData(this);

        mAdapterLesson = new GridViewAdapterLesson(MainActivity.this, mListLesson, isUpdate);
        mGridViewLesson.setAdapter(mAdapterLesson);
        mGridViewLesson.setOnItemClickListener(this);

        //catch status of textview add
        setTextAdd();

        //show into textview a week currently
        onSetWeek();

        //display table lesson from database
        displayLesson();
        //display table lesson from database
        displaySchedule(mTvCalendar.getText().toString().trim());
    }

    private void refreshDataLesson() {
        for (int i = 0; i < 15; i++) {
            mListLesson.set(i, "");
        }
        mAdapterLesson.notifyDataSetChanged();
    }

    private void refreshDataSchedule() {
        for (int i = 0; i < 36; i++) {
            mList.set(i, "");
        }
        mAdapterSchedule.notifyDataSetChanged();
    }

    private void displaySchedule(String value) {
        Cursor cursor = mDb.rawQuery("Select * From tbl_schedule where date=?", new String[]{value.trim()});
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(cursor.getString(1));
                for (int i = 0; i < jsonArray.length(); i++) {
                    mList.set(i, jsonArray.getString(i));
                }
                mAdapterSchedule.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mAdapterSchedule.notifyDataSetChanged();
    }

    private void displayLesson() {
        Cursor cursor = mDb.rawQuery("Select * From tbl_lesson", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(cursor.getString(0));
                for (int i = 0; i < jsonArray.length(); i++) {
                    mListLesson.set(i, jsonArray.getString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mAdapterLesson.notifyDataSetChanged();
        }
    }

    private boolean isCheckItem() {
        int count = 0;
        for (String item : mListLesson) {
            if (!item.equals(""))
                count++;
        }
        return count == mListLesson.size();
    }

    private void setTextAdd() {
        //update background from file resources
        mTvAdd.setBackgroundResource(isCheckItem() ? R.drawable.effect_text_disable
                : R.drawable.effect_text_enable);
        mTvAdd.setEnabled(isCheckItem() ? false : true);
    }

    private void setEnable(boolean flag) {
        mLayoutSchedule.setAlpha(isUpdate ? show_view : hide_view);
        mAdapterLesson = new GridViewAdapterLesson(MainActivity.this, mListLesson, !flag);
        mGridViewLesson.setAdapter(mAdapterLesson);
    }

    private void showCalendar() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener, mDateNow.get(Calendar.YEAR),
                mDateNow.get(Calendar.MONTH), mDateNow.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mDateNow.set(Calendar.YEAR, year);
            mDateNow.set(Calendar.MONTH, month);
            mDateNow.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            onSetWeek();

            //refresh data schedule
            refreshDataSchedule();

            //update data schedule

            displaySchedule(mTvCalendar.getText().toString().trim());
        }
    };

    /**
     * set week choose
     */
    private void onSetWeek() {
        List<Date> listDate = new ArrayList<>();
        Calendar calendar = (Calendar) mDateNow.clone();
        int number_of_days_difference = calendar.get(Calendar.DAY_OF_WEEK) - 2;

        calendar.add(Calendar.DAY_OF_WEEK, -number_of_days_difference);

        while (listDate.size() < 6) {
            listDate.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        if (listDate.size() == 6) {
            calendar.add(Calendar.DAY_OF_MONTH, 2);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String firstDayOfWeek = sdf.format(listDate.get(0).getTime());
        String lastDayOfWeek = sdf.format(listDate.get(5).getTime());
        mTvCalendar.setText(firstDayOfWeek + " - " + lastDayOfWeek);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add:
                showDialog("");
                break;
            case R.id.tv_update:
//                showDialog();
                setEnable(isUpdate);
                //event clicl update
                isClickUpdate(isUpdate);
                if (mTvUpdate.getText().toString().equals("Update")) {
                    displayLesson();
                    mLayoutdb.setVisibility(View.GONE);
                }
                //update variable isUpdate
                isUpdate = isUpdate ? false : true;

                break;
            case R.id.tv_ok:

                saveDataLesson(mListLesson);

                saveDataSchedule(mList);

                hieAndShowView(false);

                Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_cancel:

                finish();

                break;
            case R.id.img_previous:
                mDateNow.add(Calendar.DAY_OF_MONTH, -7);
                //refresh data
                refreshDataSchedule();
                onSetWeek();
                //update data schedule
                displaySchedule(mTvCalendar.getText().toString().trim());
                break;
            case R.id.img_next:
                mDateNow.add(Calendar.DAY_OF_MONTH, 7);
                //refresh data
                refreshDataSchedule();
                onSetWeek();
                //update data table schedule
                displaySchedule(mTvCalendar.getText().toString().trim());
                break;
            case R.id.tv_calendar:
                showCalendar();
                //refresh data
                refreshDataSchedule();
                //update data table schedule
                displaySchedule(mTvCalendar.getText().toString().trim());
                break;
        }
    }

    private void isClickUpdate(boolean flag) {

        mTvUpdate.setText(isUpdate ? "Update" : "Cancel");
        mTvUpdate.setBackgroundDrawable(isUpdate ? getResources().getDrawable(R.drawable.effect_text_enable) :
                getResources().getDrawable(R.drawable.effect_text_disable));
    }

    private boolean isCheckData() {
        Cursor cMain = mDb.rawQuery("Select * From tbl_lesson", null);
        cMain.moveToFirst();

        return cMain.getCount() < 15;
    }

    void showDialog(final String value) {

        AlertDialog.Builder al = new AlertDialog.Builder(this);
        //call viewgroup
        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.VERTICAL);
        //set width and height for viewgroup
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(50, 100, 50, 0);
        //call editext

        final EditText input = new EditText(this);
        input.setLayoutParams(lp);
        input.setLines(1);
        //set text for editText
        input.setText(value.isEmpty() ? null : value);
        input.setMaxLines(1);
        input.setTextSize(15);
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        input.setHint("Input Lesson Name...");
        container.addView(input, lp);

        //add container in viewgroup
        al.setView(container);
        //event click ok
        al.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isCheckData()) {
                    {
                        addItemLessonNanme(input.getText().toString().trim().replaceAll(" ", ""));
                    }
                } else {
                    Snackbar.make(mTvAdd, "Maximum of 15 lessons", Snackbar.LENGTH_SHORT).show();
                }
            }

            //envent cancel dialog
        }).setNegativeButton(Html.fromHtml("<font color='red'>No</font>"), new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        //create dialog
        Dialog dialog = al.show();
        //EditText in Dialog doesn't pull up soft keyboard
        dialog.getWindow().

                clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        ((AlertDialog) dialog).

                getButton(AlertDialog.BUTTON_NEGATIVE).

                setTextColor(Color.RED);
        //        al.create().show();
    }

    private void digalogUpdate(final int postion, final String value) {
        AlertDialog.Builder al = new AlertDialog.Builder(this);
        //not cancelable
        al.setCancelable(false);
        //call viewgroup
        View view = LayoutInflater.from(this).inflate(R.layout.item_update, null);

        TextView tv = view.findViewById(R.id.item_tv_update);
        tv.setText(value);
        final EditText edt = view.findViewById(R.id.edt_update_data);

        TextView tv_ok = view.findViewById(R.id.tv_yes);

        al.setView(view);

        final Dialog dialog = al.show();
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isExistLesson(formatText(edt.getText().toString()))) {
                    mListLesson.set(postion, formatText(edt.getText().toString()));
                    mAdapterLesson.notifyDataSetChanged();
                    //cancel dialog
                    dialog.cancel();
                    //
                    mLayoutdb.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(MainActivity.this, "Data has not changed", Toast.LENGTH_SHORT).show();
                }

            }
        });

        TextView tv_cancel = view.findViewById(R.id.tv_no);

        //event cancel dialog
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

    private String formatText(String value) {
        return value.trim().replaceAll(" ", "");
    }

    private boolean isExistLesson(String value) {
        for (int i = 0; i < mListLesson.size(); i++) {
            if (value.toLowerCase().equals(mListLesson.get(i).toLowerCase()))
                return false;
        }
        return true;
    }

    private String datePickerDialog() {

        final String[] date = {""};


        return date[0];
    }

    //methed condition add item lesson name
    private void addItemLessonNanme(final String value) {
        boolean erro = true;
        for (int i = 0; i < mListLesson.size(); i++) {
            if (mListLesson.get(i).equals("") && isExistLesson(value)) {
                mListLesson.set(i, value);
                mAdapterLesson.notifyDataSetChanged();
                //add database
                saveDataLesson(mListLesson);
                erro = false;
                break;
            }
        }
        //notification if fail
        if (erro) {
            Snackbar.make(mTvAdd, "Lesson name already exists", Snackbar.LENGTH_SHORT).show();
        }
        //check text button add lesson name
        setTextAdd();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //sự kiện của two gridview
        switch (parent.getId()) {
            case R.id.gridViewLesson:
                if (isUpdate) {
                    digalogUpdate(position, mListLesson.get(position));
                } else {
                    //not show no information
                }
                break;
            case R.id.gridViewSchedule:
                Toast.makeText(this, "Schedule", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale,
                startScale, endScale,
                Animation.RELATIVE_TO_SELF, 1.0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1.0f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(DURATION);
        v.startAnimation(anim);
    }

    private boolean isCheckSchedule(final String value) {
        Cursor cursor = mDb.rawQuery("Select * From tbl_schedule where date=?", new String[]{value});
        cursor.moveToFirst();
        return cursor.getCount() > 0;
    }

    @Override
    public void setOnItemSchedule(int index, int postion, String value) {
        mList.set(index, "");
        mList.set(postion, value);
        mAdapterSchedule.notifyDataSetChanged();
    }

    @Override
    public void setOnItemLesson(final int postion, final String value) {

        AlertDialog.Builder al = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.item_config, null);

        final LinearLayout layout = view.findViewById(R.id.layout_pick_date);

        AppCompatCheckBox checkBox = view.findViewById(R.id.appCheckBox);
        checkBox.setChecked(preferences.getOption());
        //check cycle
        layout.setVisibility(preferences.getOption() ? View.VISIBLE : View.GONE);

        final TextView tvFromDate = view.findViewById(R.id.tv_fromDate);

        final TextView tvTodate = view.findViewById(R.id.tv_toDate);

        TextView tv_No = view.findViewById(R.id.tv_no);

        TextView tv_Yes = view.findViewById(R.id.tv_yes);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(MainActivity.this, "" + isChecked, Toast.LENGTH_SHORT).show();
                preferences.setOption(isChecked);
                layout.setVisibility(preferences.getOption() ? View.VISIBLE : View.GONE);
            }
        });

        tvFromDate.setText("From :" + getDateCurrent());

        tvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                DatePickerDialog.OnDateSetListener event = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);

                        tvFromDate.setText("From :" + sdf.format(calendar.getTime()));
                    }
                };
                //B2: Gọi hiển thị

                Date currentTime = Calendar.getInstance().getTime();

                DatePickerDialog dl = new DatePickerDialog(MainActivity.this, event, Integer.parseInt(sdf.format(currentTime).split("/")[2]),
                        Integer.parseInt(sdf.format(currentTime).split("/")[1]), Integer.parseInt(sdf.format(currentTime).split("/")[0]));
                dl.setTitle("Chose Date!");
                dl.show();

            }
        });

        tvTodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                DatePickerDialog.OnDateSetListener event = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);

                        tvTodate.setText("To :" + sdf.format(calendar.getTime()));
                    }
                };
                //B2: Gọi hiển thị

                Date currentTime = Calendar.getInstance().getTime();

                DatePickerDialog dl = new DatePickerDialog(MainActivity.this, event, Integer.parseInt(sdf.format(currentTime).split("/")[2]),
                        Integer.parseInt(sdf.format(currentTime).split("/")[1]), Integer.parseInt(sdf.format(currentTime).split("/")[0]));
                dl.setTitle("Chose Date!");
                dl.show();
            }
        });

        al.setView(view);

        final Dialog dialog = al.show();

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        tv_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.set(postion, value);
                mAdapterSchedule.notifyDataSetChanged();

                //mlist cycle
                mListCycle.set(postion, value);
                dialog.cancel();
            }
        });
    }

    private boolean chectDate(String value1, String value2) {
        return value1.equals(value2);
    }

    private String getDateCurrent() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

    private void saveDataLesson(ArrayList<String> list) {
        Cursor cursor = mDb.rawQuery("Select * From tbl_lesson", null);
        cursor.moveToFirst();
        JSONArray jsonArray = new JSONArray(list);
        ContentValues contentValues = new ContentValues();
        contentValues.put("data", jsonArray.toString());
        if (cursor.getCount() > 0)
            mDb.update("tbl_lesson", contentValues, null, null);
        else {
            mDb.insert("tbl_lesson", null, contentValues);
        }

    }

    private void saveDataSchedule(ArrayList<String> list) {
        JSONArray array = new JSONArray(list);
        if (isCheckSchedule(mTvCalendar.getText().toString().trim())) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("data", array.toString());
            contentValues.put("data", array.toString());
            mDb.update("tbl_schedule", contentValues, "date=?", new String[]{mTvCalendar.getText().toString()});
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("date", mTvCalendar.getText().toString());
            contentValues.put("data", array.toString());
            mDb.insert("tbl_schedule", null, contentValues);
        }

    }

    @Override
    public void setOnChangeData(boolean flag) {
        hieAndShowView(flag);
    }

    private void hieAndShowView(final boolean flag) {
        mLayoutdb.animate()
                .alpha(flag ? 1.0f : 0.f)
                .setDuration(700)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutdb.setVisibility(flag ? View.VISIBLE : View.GONE);
                    }
                });

    }

    class DragToTrashListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    scaleView(mImgRecycle, 1.0f, 1.2f);
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();
                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;
                    //view being dragged and dropped
                    TextView dropped = (TextView) view;
                    //checking whether first character of dropTarget equals first character of dropped
                    if (dropTarget.getId() == R.id.img_recycle) {

                        view.setVisibility(View.VISIBLE);

                        //if text !=null then active
                        if (!dropped.getText().toString().trim().equals("")) {
                            removeItem(dropped.getId(), dropped.getText().toString());
                        }

                        dropped.setText("");

                    } else {
                        //displays message if first character of dropTarget is not equal to first character of dropped
                        Toast.makeText(MainActivity.this, "", Toast.LENGTH_LONG).show();
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //event end drag view
                    scaleView(mImgRecycle, 1.2f, 1.0f);
                    break;
                default:
                    break;
            }
            return true;
        }
    }


    //methed delete item
    private void removeItem(int id, String value) {
        switch (id) {
            case R.id.item_tv_lesson:
                for (int i = 0; i < mListLesson.size(); i++) {
                    if (value.equals(mListLesson.get(i))) {
                        mListLesson.set(i, "");
                        //save data
                        saveDataLesson(mListLesson);
                        mAdapterLesson.notifyDataSetChanged();
                        break;
                    }
                }
                break;
            case R.id.item_tv_schedule:
                for (int i = 0; i < mList.size(); i++) {
                    if (value.equals(mList.get(i))) {
                        mList.set(i, "");
                        //save data in database
                        JSONArray array = new JSONArray(mList);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("data", array.toString());
                        contentValues.put("data", array.toString());
                        mDb.update("tbl_schedule", contentValues, "date=?", new String[]{mTvCalendar.getText().toString()});
                        mAdapterSchedule.notifyDataSetChanged();
                        break;
                    }
                }
                break;
        }
    }
}
