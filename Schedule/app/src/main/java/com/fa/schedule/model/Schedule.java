package com.fa.schedule.model;

import java.io.Serializable;

public class Schedule implements Serializable {
    private String date, data;

    public Schedule(String date, String data) {
        this.date = date;
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return date + "#" + data;
    }
}
